<?php namespace Fifteen\Backups;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class BackupsServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $providers = [
            \Spatie\Backup\BackupServiceProvider::class
        ];
        foreach ($providers as $item) {
            $this->app->register($item);
        }
    }

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/resources/views', 'Backups');
        if (! $this->app->routesAreCached()) {
            require __DIR__ . '/Http/routes.php';
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['fifteen.backups'];
    }

}

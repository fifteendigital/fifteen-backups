@extends('Template::layouts.master')

@section('page_title')

    <h3 class="page-title">{{ Alang::get('general.database_backups') }}</h3>

@endsection

@section('content')

    <div class="portlet light bordered">
        
        <div class="portlet-body">
            <div class="row">

                <div class="col-md-6 text-center" style="">
                    <h3 class="text-left"> {{ Alang::get('general.create_a_new_database_backup') }}:</h3>
                    <a class="btn btn-primary green large_text create_backup_btn" href="{{ route('database-backups.create') }}">
                        {{ Alang::get('general.create_backup') }} 
                        <i class="fa fa-cloud-upload large_text"></i>
                    </a>
                </div>

                <div class="col-md-6 text-center right_col">
                    <h3 class="text-left">{{ Alang::get('general.download_a_database_backup') }}:</h3>
                    {!! Form::open(['route' => 'database-backups.download', 'role' => 'form', 'class' => 'form-horizontal']) !!}
                        <select name="backup_file" class="form-control" id="backup_file" size="10">
                            @foreach ($files as $key => $name)
                                <option value="{{ $key}}">{{ $name }}</option>
                            @endforeach
                        </select>
                    {!! Form::close() !!}
                    <br />
                    <a class="btn btn-primary large_text" id="download_backup"  href="#">
                        {{ Alang::get('general.download_backup') }} 
                        <i style="font-size:25px" class="fa fa-cloud-download large_text"></i>
                    </a>
                </div>
            </div>
            
        </div>
    </div>

@endsection

@push('styles')
    <style>
        .large_text { font-size:25px;  }
        .create_backup_btn { margin-top:70px; }
        .right_col { border-left:1px solid #e1e1e1; }
    </style>
@endpush

@push('scripts')
    <script>
        $(function() {
            $('#download_backup').click(function(){
                var obj = $(this);
                var type = obj.data('name');
                $('input[name=type]').val(type);
                $('form').submit();
            });
        });
    </script>
@endpush

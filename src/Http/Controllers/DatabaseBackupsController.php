<?php namespace Fifteen\Backups\Http\Controllers;

use \App\Exceptions\ValidationException;
use \Breadcrumbs;
use \Navigation;
use \Alang;
use \View;
use \Redirect;
use Carbon\Carbon;
use \Fifteen\Charabanc\Http\Controllers\BaseController;
use Illuminate\Contracts\Filesystem\Filesystem;

class DatabaseBackupsController extends BaseController {

	protected $filesystem;

	public function __construct(Filesystem $filesystem)
	{
        $this->filesystem =
            \Config::has('laravel-backup.destination.filesystem') ?
                \Storage::disk(\Config::get('laravel-backup.destination.filesystem')[0]) :
                $filesystem;
		$this->page_name =  Alang::get('general.database_backups');

		parent::__construct();

		\Breadcrumbs::addCrumb($this->page_name, route('database-backups.index'));
	}

	public function index()
	{
		$files_list = $this->filesystem->files('backups');
		rsort($files_list);
		$files =[];
		foreach($files_list as $file) {
			$file_parts = explode("/", $file);
			$date = substr($file_parts[1], 0, strpos($file_parts[1], '.'));
            
			if (!empty($date)) {
				$files[$file] = Carbon::createFromFormat('Y-m-d-H-i-s', $date)
					->format('d/m/Y H:i');
			}

		}
		return View::make('Backups::index', compact('files'));

	}

	public function createBackup()
	{
		\Artisan::call('backup:run', [
			'--only-db' => 'true'
		]);

		return \Redirect::route('database-backups.index')->with('message', \Alang::get('general.the_backup_has_been_created') . '.');
	}

	public function downloadBackup()
	{

		$file = \Request::get('backup_file');
		if(!$file) {
			return Redirect::route('database-backups.index')->withErrors(Alang::get('general.you_must_select_a_file_to_download'));
		}
		$file_system = $this->filesystem->getDriver();
		$stream = $file_system->readStream($file);

		return \Response::stream(function() use($stream) {
		    fpassthru($stream);
		}, 200, [
		    "Content-Type" => $file_system->getMimetype($file),
		    "Content-Length" => $file_system->getSize($file),
		    "Content-disposition" => "attachment; filename=\"" . basename($file) . "\"",
		]);

	}
}

<?php
\Route::group(['namespace' => 'Fifteen\Backups\Http\Controllers', 'middleware' => ['web']], function() {

	\Route::group(['middleware' => 'has-access', 'permissions' => 'backups.all'], function() {
		Route::get('database-backups',  [
		'uses' => 'DatabaseBackupsController@index',
		    'as' => 'database-backups.index'
		]);
		Route::get('database-backups/create',  [
		    'uses' => 'DatabaseBackupsController@createBackup',
		    'as' => 'database-backups.create'
		]);
		Route::post('database-backups/download',  [
		    'uses' => 'DatabaseBackupsController@downloadBackup',
		    'as' => 'database-backups.download'
		]);
	});
});

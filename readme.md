# Backups

A basic package which allows users to take SQL dumps of the database. Works with AWS S3. 

It makes use of the [Laravel-backup](https://github.com/spatie/laravel-backup) package.

This requires Charabanc to be installed.

### Installation

Add Backups to your composer.json file:

```
    "require": {
        "php": ">=5.5.9",
        "laravel/framework": "5.1.*",
        "fifteen/backups": "1.0.*"
    },
```

Add minimum-stability: dev:
```sh
    "minimum-stability": "dev",
```

As the repository isn't on Packagist, you also need to include it in the repositories list:

```php
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:fifteendigital/fifteen-backups.git"
        }
    ]
```

Run composer update:

```sh
> composer update
```

Register service provider in config/app.php:

```php
    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        ...

        /*
         * Package Service Providers...
         */
        Fifteen\Backups\BackupsServiceProvider::class,
    ];
```

### Publish config 

```sh
php artisan vendor:publish --provider="Spatie\Backup\BackupServiceProvider"
```

If using S3 (see above), you need to update config/laravel-backup.php to use it too:
```sh
    'destination' => [

        'filesystem' => ['s3'],
```

### Set up a cron

```sh
php artisan backup:run --only-db
```

### Add to navigation

```php
    'Database backups' => [
        'route' => 'database-backups.index',
        'lang' => 'general.database_backups',
    ],
```

### Security issues

You need to ensure that the backup files are not accessible outside of the backups screens.

One potential vulnerability is the files routes, so you may need to create appropriate middleware or some way of preventing direct access to the backup files:

```php
    \Route::get('/files/backups/{path?}', function() {
        \App::abort(404);  // Backups should not be accessible via routes
    });
    \Route::get('/images/backups/{path?}', function() {
        \App::abort(404);  // Backups should not be accessible via routes
    });
```

